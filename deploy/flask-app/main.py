# coding:utf-8
from flask import Flask
from flask_redis import FlaskRedis
import redis
r = redis.Redis(host='redis-svc', port=6379, db=0)
# REDIS_URL = "redis://:password@redis-svc:6379/0"

app = Flask(__name__)
# redis_store = FlaskRedis(app)


@app.route('/')
def hello_word():
    r.incr("flask")
    return r.get("flask")


if __name__ == '__main__':
    app.run(host='0.0.0.0')
